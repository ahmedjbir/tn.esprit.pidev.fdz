package tn.esprit.pidev.fdz.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import tn.esprit.pidev.fdz.entities.Sharing;
import tn.esprit.pidev.fdz.services.interfaces.SharingCrudLocal;
import tn.esprit.pidev.fdz.services.interfaces.SharingCrudRemote;

/**
 * Session Bean implementation class SharingDao
 */
@Stateless
public class SharingCrud implements SharingCrudRemote, SharingCrudLocal {
	
	@PersistenceContext
    private EntityManager em;
	
	Sharing sharing;

    /**
     * Default constructor. 
     */
    public SharingCrud() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Sharing sharing) {
		em.persist(sharing);		
	}

	@Override
	public void edit(Sharing sharing) {
		em.merge(sharing);
	}

	@Override
	public void remove(Sharing sharing) {
		em.remove(em.merge(sharing));		
	}

	@Override
	public Sharing find(int id) {
		return em.find(Sharing.class, id);
	}

	@Override
	public List<Sharing> findAll() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		cq.select(cq.from(Sharing.class));
		return em.createQuery(cq).getResultList();
	}

	@Override
	public int count() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		Root<Sharing> rt = cq.from(Sharing.class);
		cq.select(em.getCriteriaBuilder().count(rt));
		Query q = em.createQuery(cq);
		return ((Long) q.getSingleResult()).intValue();
	}

}
