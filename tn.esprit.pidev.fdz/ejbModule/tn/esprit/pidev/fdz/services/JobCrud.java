package tn.esprit.pidev.fdz.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import tn.esprit.pidev.fdz.entities.Job;
import tn.esprit.pidev.fdz.services.interfaces.JobCrudLocal;
import tn.esprit.pidev.fdz.services.interfaces.JobCrudRemote;

/**
 * Session Bean implementation class JobDao
 */
@Stateless
public class JobCrud implements JobCrudRemote, JobCrudLocal {
	
	@PersistenceContext
    private EntityManager em;
	
	Job job;

    /**
     * Default constructor. 
     */
    public JobCrud() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Job job) {
		em.persist(job);		
	}

	@Override
	public void edit(Job job) {
		em.merge(job);
	}

	@Override
	public void remove(Job job) {
		em.remove(em.merge(job));		
	}

	@Override
	public Job find(int id) {
		return em.find(Job.class, id);
	}

	@Override
	public List<Job> findAll() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		cq.select(cq.from(Job.class));
		return em.createQuery(cq).getResultList();
	}

	@Override
	public int count() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		Root<Job> rt = cq.from(Job.class);
		cq.select(em.getCriteriaBuilder().count(rt));
		Query q = em.createQuery(cq);
		return ((Long) q.getSingleResult()).intValue();
	}
	
	public List<Object[]> groupByCategory() {
		Query query = em.createNamedQuery("Job.GroupByCategory");
		return query.getResultList();
	}
	
	public List<Object[]> groupByRate() {
		Query query = em.createNamedQuery("Job.GroupByRate");
		return query.getResultList();
	}
	
	public List<Job> findByEtat(int etat)
    {
        List <Job> jobs = null;
        try{
            
        Query query = em.createNamedQuery("Job.findByEtat");
        query.setParameter("etat", etat);
        
        jobs = (List<Job>)query.getResultList();
        return jobs;
        }
        catch(NoResultException e){
            return jobs;
        }
    }

}
