package tn.esprit.pidev.fdz.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import tn.esprit.pidev.fdz.entities.Administrator;
import tn.esprit.pidev.fdz.services.interfaces.AdministratorCrudLocal;
import tn.esprit.pidev.fdz.services.interfaces.AdministratorCrudRemote;

/**
 * Session Bean implementation class AdministratorDao
 */
@Stateless
public class AdministratorCrud implements AdministratorCrudRemote, AdministratorCrudLocal {

	@PersistenceContext
    private EntityManager em;
	
	Administrator administrator;

    /**
     * Default constructor. 
     */
    public AdministratorCrud() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Administrator administrator) {
		em.persist(administrator);		
	}

	@Override
	public void edit(Administrator administrator) {
		em.merge(administrator);
	}

	@Override
	public void remove(Administrator administrator) {
		em.remove(em.merge(administrator));		
	}

	@Override
	public Administrator find(int id) {
		return em.find(Administrator.class, id);
	}

	@Override
	public List<Administrator> findAll() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		cq.select(cq.from(Administrator.class));
		return em.createQuery(cq).getResultList();
	}

	@Override
	public int count() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		Root<Administrator> rt = cq.from(Administrator.class);
		cq.select(em.getCriteriaBuilder().count(rt));
		Query q = em.createQuery(cq);
		return ((Long) q.getSingleResult()).intValue();
	}
	
	 public Administrator findByLoginAndPassword(String login, String password)
	    {
	        Administrator administrator = null;
	        
	        try{
	            
	        Query query = em.createNamedQuery("Administrator.findByLoginAndPassword");
	        query.setParameter("login", login);
	        query.setParameter("password", password);
	        
	        administrator = (Administrator) query.getSingleResult();
	        return administrator;
	        }
	        catch(NoResultException e){
	            return administrator;
	        }
	    }

}
