package tn.esprit.pidev.fdz.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import tn.esprit.pidev.fdz.entities.User;
import tn.esprit.pidev.fdz.services.interfaces.UserCrudLocal;
import tn.esprit.pidev.fdz.services.interfaces.UserCrudRemote;

/**
 * Session Bean implementation class UserDao
 */
@Stateless
public class UserCrud implements UserCrudRemote, UserCrudLocal {
	
	@PersistenceContext
    private EntityManager em;
	
	User user;

    /**
     * Default constructor. 
     */
    public UserCrud() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(User user) {
		em.persist(user);		
	}

	@Override
	public void edit(User user) {
		em.merge(user);
	}

	@Override
	public void remove(User user) {
		em.remove(em.merge(user));		
	}

	@Override
	public User find(int id) {
		return em.find(User.class, id);
	}

	@Override
	public List<User> findAll() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		cq.select(cq.from(User.class));
		return em.createQuery(cq).getResultList();
	}

	@Override
	public int count() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		Root<User> rt = cq.from(User.class);
		cq.select(em.getCriteriaBuilder().count(rt));
		Query q = em.createQuery(cq);
		return ((Long) q.getSingleResult()).intValue();
	}
	
	public List<User> findByLogin(String login)
    {
        List<User> users = null;
        
        try{
            
        Query query = em.createNamedQuery("User.findByLogin");
        query.setParameter("login", login);
      //  List<User> users;
        return query.getResultList();
      
        }
        catch(NoResultException e){
        	return users;
        //	System.out.println("La liste est vide !");
        }
    }

}
