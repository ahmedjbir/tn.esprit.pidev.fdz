package tn.esprit.pidev.fdz.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import tn.esprit.pidev.fdz.entities.Message;
import tn.esprit.pidev.fdz.services.interfaces.MessageCrudLocal;
import tn.esprit.pidev.fdz.services.interfaces.MessageCrudRemote;

/**
 * Session Bean implementation class MessageDao
 */
@Stateless
public class MessageCrud implements MessageCrudRemote, MessageCrudLocal {
	
	@PersistenceContext
    private EntityManager em;
	
	Message message;

    /**
     * Default constructor. 
     */
    public MessageCrud() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Message message) {
		em.persist(message);		
	}

	@Override
	public void edit(Message message) {
		em.merge(message);
	}

	@Override
	public void remove(Message message) {
		em.remove(em.merge(message));		
	}

	@Override
	public Message find(int id) {
		return em.find(Message.class, id);
	}

	@Override
	public List<Message> findAll() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		cq.select(cq.from(Message.class));
		return em.createQuery(cq).getResultList();
	}

	@Override
	public int count() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		Root<Message> rt = cq.from(Message.class);
		cq.select(em.getCriteriaBuilder().count(rt));
		Query q = em.createQuery(cq);
		return ((Long) q.getSingleResult()).intValue();
	}

}
