package tn.esprit.pidev.fdz.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import tn.esprit.pidev.fdz.entities.Transaction;
import tn.esprit.pidev.fdz.services.interfaces.TransactionCrudLocal;
import tn.esprit.pidev.fdz.services.interfaces.TransactionCrudRemote;

/**
 * Session Bean implementation class TransactionDao
 */
@Stateless
public class TransactionCrud implements TransactionCrudRemote, TransactionCrudLocal {
	
	@PersistenceContext
    private EntityManager em;
	
	Transaction transaction;

    /**
     * Default constructor. 
     */
    public TransactionCrud() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Transaction transaction) {
		em.persist(transaction);		
	}

	@Override
	public void edit(Transaction transaction) {
		em.merge(transaction);
	}

	@Override
	public void remove(Transaction transaction) {
		em.remove(em.merge(transaction));		
	}

	@Override
	public Transaction find(int id) {
		return em.find(Transaction.class, id);
	}

	@Override
	public List<Transaction> findAll() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		cq.select(cq.from(Transaction.class));
		return em.createQuery(cq).getResultList();
	}

	@Override
	public int count() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		Root<Transaction> rt = cq.from(Transaction.class);
		cq.select(em.getCriteriaBuilder().count(rt));
		Query q = em.createQuery(cq);
		return ((Long) q.getSingleResult()).intValue();
	}

}
