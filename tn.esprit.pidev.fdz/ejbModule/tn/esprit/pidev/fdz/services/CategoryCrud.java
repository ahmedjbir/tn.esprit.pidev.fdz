package tn.esprit.pidev.fdz.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import tn.esprit.pidev.fdz.entities.Category;
import tn.esprit.pidev.fdz.services.interfaces.CategoryCrudLocal;
import tn.esprit.pidev.fdz.services.interfaces.CategoryCrudRemote;

/**
 * Session Bean implementation class CategoryDao
 */
@Stateless
public class CategoryCrud implements CategoryCrudRemote, CategoryCrudLocal {
	
	@PersistenceContext
    private EntityManager em;
	
	//Category category;

    /**
     * Default constructor. 
     */
    public CategoryCrud() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Category category) {
		em.persist(category);		
	}

	@Override
	public void edit(Category category) {
		em.merge(category);
	}

	@Override
	public void remove(Category category) {
		em.remove(em.find(Category.class, category.getId()));		
	}

	@Override
	public Category find(int id) {
		return em.find(Category.class, id);
	}

	@Override
	public List<Category> findAll() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		cq.select(cq.from(Category.class));
		return em.createQuery(cq).getResultList();
	}

	@Override
	public int count() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		Root<Category> rt = cq.from(Category.class);
		cq.select(em.getCriteriaBuilder().count(rt));
		Query q = em.createQuery(cq);
		return ((Long) q.getSingleResult()).intValue();
	}

	@Override
	public List<Category> ListCategory() {
		Query query=em.createQuery("select c from Category c where c.idCategory is null");
		return query.getResultList();
	}

	@Override
	public List<Category> ListSubCategory(Category idCategory) {
		Query query=em.createQuery("select c from Category c WHERE c.idCategory = :id");
		query.setParameter("id", idCategory);
     	return query.getResultList();
	}

	@Override
	public Category findByName(String name) {
		Query query=em.createQuery("select c from Category c WHERE c.name = :name_cat");
		query.setParameter("name_cat", name);
		return (Category) query.getSingleResult();
	}


	
}
