package tn.esprit.pidev.fdz.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import tn.esprit.pidev.fdz.entities.JobOwner;
import tn.esprit.pidev.fdz.services.interfaces.JobOwnerCrudLocal;
import tn.esprit.pidev.fdz.services.interfaces.JobOwnerCrudRemote;

/**
 * Session Bean implementation class JobOwnerCrud
 */
@Stateless
public class JobOwnerCrud implements JobOwnerCrudRemote, JobOwnerCrudLocal {

	@PersistenceContext
    private EntityManager em;
	
	JobOwner jobOwner;

    /**
     * Default constructor. 
     */
    public JobOwnerCrud() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(JobOwner jobOwner) {
		em.persist(jobOwner);		
	}

	@Override
	public void edit(JobOwner jobOwner) {
		em.merge(jobOwner);
	}

	@Override
	public void remove(JobOwner jobOwner) {
		em.remove(em.merge(jobOwner));		
	}

	@Override
	public JobOwner find(int id) {
		return em.find(jobOwner.getClass(), id);
	}

	@Override
	public List<JobOwner> findAll() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		cq.select(cq.from(JobOwner.class));
		return em.createQuery(cq).getResultList();
	}

	@Override
	public int count() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		Root<JobOwner> rt = cq.from(JobOwner.class);
		cq.select(em.getCriteriaBuilder().count(rt));
		Query q = em.createQuery(cq);
		return ((Long) q.getSingleResult()).intValue();
	}

}
