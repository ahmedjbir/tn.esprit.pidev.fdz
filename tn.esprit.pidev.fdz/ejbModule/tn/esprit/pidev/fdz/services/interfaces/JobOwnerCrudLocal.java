package tn.esprit.pidev.fdz.services.interfaces;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.pidev.fdz.entities.JobOwner;

@Local
public interface JobOwnerCrudLocal {
	
	public void create(JobOwner jobOwner);
	public void edit(JobOwner jobOwner);
	public void remove(JobOwner jobOwner);
	public JobOwner find(int id);
	public List<JobOwner> findAll();
	public int count();


}
