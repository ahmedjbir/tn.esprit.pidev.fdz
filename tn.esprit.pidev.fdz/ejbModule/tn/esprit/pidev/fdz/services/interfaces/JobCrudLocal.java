package tn.esprit.pidev.fdz.services.interfaces;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.pidev.fdz.entities.Job;

@Local
public interface JobCrudLocal {
	
	public void create(Job job);
	public void edit(Job job);
	public void remove(Job job);
	public Job find(int id);
	public List<Job> findAll();
	public int count();
	public List<Object[]> groupByCategory();
	public List<Object[]> groupByRate();
	public List<Job> findByEtat(int etat);

}
