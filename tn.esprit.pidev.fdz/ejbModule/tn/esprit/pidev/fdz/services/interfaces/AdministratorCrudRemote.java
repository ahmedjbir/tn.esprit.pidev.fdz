package tn.esprit.pidev.fdz.services.interfaces;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.pidev.fdz.entities.Administrator;

@Remote
public interface AdministratorCrudRemote {
	
	public void create(Administrator administrator);
	public void edit(Administrator administrator);
	public void remove(Administrator administrator);
	public Administrator find(int id);
	public List<Administrator> findAll();
	public int count();
	public Administrator findByLoginAndPassword(String login, String password);

}
