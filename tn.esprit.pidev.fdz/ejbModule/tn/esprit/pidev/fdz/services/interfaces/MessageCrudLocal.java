package tn.esprit.pidev.fdz.services.interfaces;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.pidev.fdz.entities.Message;;

@Local
public interface MessageCrudLocal {
	
	public void create(Message message);
	public void edit(Message message);
	public void remove(Message message);
	public Message find(int id);
	public List<Message> findAll();
	public int count();


}
