package tn.esprit.pidev.fdz.services.interfaces;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.pidev.fdz.entities.Transaction;

@Remote
public interface TransactionCrudRemote {
	
	public void create(Transaction transaction);
	public void edit(Transaction transaction);
	public void remove(Transaction transaction);
	public Transaction find(int id);
	public List<Transaction> findAll();
	public int count();

}
