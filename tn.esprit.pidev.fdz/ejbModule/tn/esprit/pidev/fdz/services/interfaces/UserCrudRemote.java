package tn.esprit.pidev.fdz.services.interfaces;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.pidev.fdz.entities.User;

@Remote
public interface UserCrudRemote {
	
	public void create(User user);
	public void edit(User user);
	public void remove(User user);
	public User find(int id);
	public List<User> findAll();
	public int count();
	public List<User> findByLogin(String login);

}
