package tn.esprit.pidev.fdz.services.interfaces;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.pidev.fdz.entities.AddRequest;

@Remote
public interface AddRequestCrudRemote {
	
	public void create(AddRequest addRequest);
	public void edit(AddRequest addRequest);
	public void remove(AddRequest addRequest);
	public AddRequest find(int id);
	public List<AddRequest> findAll();
	public int count();

}
