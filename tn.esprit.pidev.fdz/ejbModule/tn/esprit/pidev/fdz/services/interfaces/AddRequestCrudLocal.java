package tn.esprit.pidev.fdz.services.interfaces;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.pidev.fdz.entities.AddRequest;;

@Local
public interface AddRequestCrudLocal {
	
	public void create(AddRequest addRequest);
	public void edit(AddRequest addRequest);
	public void remove(AddRequest addRequest);
	public AddRequest find(int id);
	public List<AddRequest> findAll();
	public int count();

}
