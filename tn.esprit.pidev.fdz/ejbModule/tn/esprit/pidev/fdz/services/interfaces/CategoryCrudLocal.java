package tn.esprit.pidev.fdz.services.interfaces;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.pidev.fdz.entities.Category;;

@Local
public interface CategoryCrudLocal {
	
	public void create(Category category);
	public void edit(Category category);
	public void remove(Category category);
	public Category find(int id);
	public List<Category> findAll();
	public int count();
	public List<Category> ListCategory();
	public List<Category> ListSubCategory(Category idCategory);
	public Category findByName(String name);

}
