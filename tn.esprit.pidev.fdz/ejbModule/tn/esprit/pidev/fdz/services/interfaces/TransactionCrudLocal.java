package tn.esprit.pidev.fdz.services.interfaces;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.pidev.fdz.entities.Transaction;;

@Local
public interface TransactionCrudLocal {
	
	public void create(Transaction transaction);
	public void edit(Transaction transaction);
	public void remove(Transaction transaction);
	public Transaction find(int id);
	public List<Transaction> findAll();
	public int count();

}
