package tn.esprit.pidev.fdz.services.interfaces;

import javax.ejb.Remote;

@Remote
public interface MailRemote {
	
	 public void sendMail(String toAddress, String subject, String content);

}
