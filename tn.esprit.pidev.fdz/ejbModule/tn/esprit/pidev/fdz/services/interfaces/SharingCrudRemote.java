package tn.esprit.pidev.fdz.services.interfaces;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.pidev.fdz.entities.Sharing;

@Remote
public interface SharingCrudRemote {
	
	public void create(Sharing sharing);
	public void edit(Sharing sharing);
	public void remove(Sharing sharing);
	public Sharing find(int id);
	public List<Sharing> findAll();
	public int count();

}
