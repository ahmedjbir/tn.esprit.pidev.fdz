package tn.esprit.pidev.fdz.services.interfaces;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.pidev.fdz.entities.Sharing;;

@Local
public interface SharingCrudLocal {
	
	public void create(Sharing sharing);
	public void edit(Sharing sharing);
	public void remove(Sharing sharing);
	public Sharing find(int id);
	public List<Sharing> findAll();
	public int count();

}
