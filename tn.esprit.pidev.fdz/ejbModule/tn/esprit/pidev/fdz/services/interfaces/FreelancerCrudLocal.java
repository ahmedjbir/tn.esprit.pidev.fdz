package tn.esprit.pidev.fdz.services.interfaces;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.pidev.fdz.entities.Freelancer;

@Local
public interface FreelancerCrudLocal {
	
	public void create(Freelancer freelancer);
	public void edit(Freelancer freelancer);
	public void remove(Freelancer freelancer);
	public Freelancer find(int id);
	public List<Freelancer> findAll();
	public int count();

}
