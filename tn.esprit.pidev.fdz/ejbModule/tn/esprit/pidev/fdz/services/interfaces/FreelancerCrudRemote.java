package tn.esprit.pidev.fdz.services.interfaces;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.pidev.fdz.entities.Freelancer;

@Remote
public interface FreelancerCrudRemote {
	
	public void create(Freelancer freelancer);
	public void edit(Freelancer freelancer);
	public void remove(Freelancer freelancer);
	public Freelancer find(int id);
	public List<Freelancer> findAll();
	public int count();

}
