package tn.esprit.pidev.fdz.services;

import java.io.Serializable;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.annotation.Resource;
import javax.ejb.Stateless;

import tn.esprit.pidev.fdz.services.interfaces.MailRemote;

@Stateless
public class Mail implements MailRemote {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Resource(lookup = "java:jboss/mail/Default")
    private Session mailSession;
	
	
	public void sendMail(String toAddress, String subject, String content)
	{
		
		try {
            MimeMessage m = new MimeMessage(mailSession);
            Address from = new InternetAddress("freedevzone@gmail.com");
            Address[] to = new InternetAddress[] { new InternetAddress(
                toAddress) };
            m.setFrom(from);
            m.setRecipients(Message.RecipientType.TO, to);
            m.setSubject(subject);
            m.setContent(content, "text/plain");
            Transport.send(m);

            System.out.println("Mail Sent Successfully.");
        } catch (javax.mail.MessagingException e) {
            e.printStackTrace();
        }
		
	}
}
