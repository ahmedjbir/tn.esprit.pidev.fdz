package tn.esprit.pidev.fdz.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import tn.esprit.pidev.fdz.entities.AddRequest;
import tn.esprit.pidev.fdz.services.interfaces.AddRequestCrudLocal;
import tn.esprit.pidev.fdz.services.interfaces.AddRequestCrudRemote;

/**
 * Session Bean implementation class AddRequestDao
 */
@Stateless
public class AddRequestCrud implements AddRequestCrudRemote, AddRequestCrudLocal {
	
	@PersistenceContext
    private EntityManager em;
	
	AddRequest addRequest;

    /**
     * Default constructor. 
     */
    public AddRequestCrud() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(AddRequest addRequest) {
		em.persist(addRequest);		
	}

	@Override
	public void edit(AddRequest addRequest) {
		em.merge(addRequest);
	}

	@Override
	public void remove(AddRequest addRequest) {
		em.remove(em.merge(addRequest));		
	}

	@Override
	public AddRequest find(int id) {
		return em.find(AddRequest.class, id);
	}

	@Override
	public List<AddRequest> findAll() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		cq.select(cq.from(AddRequest.class));
		return em.createQuery(cq).getResultList();
	}

	@Override
	public int count() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		Root<AddRequest> rt = cq.from(AddRequest.class);
		cq.select(em.getCriteriaBuilder().count(rt));
		Query q = em.createQuery(cq);
		return ((Long) q.getSingleResult()).intValue();
	}

}
