package tn.esprit.pidev.fdz.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import tn.esprit.pidev.fdz.entities.Freelancer;
import tn.esprit.pidev.fdz.services.interfaces.FreelancerCrudLocal;
import tn.esprit.pidev.fdz.services.interfaces.FreelancerCrudRemote;

/**
 * Session Bean implementation class FreelancerDao
 */
@Stateless
public class FreelancerCrud implements FreelancerCrudRemote, FreelancerCrudLocal {

	@PersistenceContext
    private EntityManager em;
	
	Freelancer freelancer;

    /**
     * Default constructor. 
     */
    public FreelancerCrud() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Freelancer freelancer) {
		em.persist(freelancer);		
	}

	@Override
	public void edit(Freelancer freelancer) {
		em.merge(freelancer);
	}

	@Override
	public void remove(Freelancer freelancer) {
		em.remove(em.merge(freelancer));		
	}

	@Override
	public Freelancer find(int id) {
		return em.find(freelancer.getClass(), id);
	}

	@Override
	public List<Freelancer> findAll() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		cq.select(cq.from(Freelancer.class));
		return em.createQuery(cq).getResultList();
	}

	@Override
	public int count() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		Root<Freelancer> rt = cq.from(Freelancer.class);
		cq.select(em.getCriteriaBuilder().count(rt));
		Query q = em.createQuery(cq);
		return ((Long) q.getSingleResult()).intValue();
	}

}
