package tn.esprit.pidev.fdz.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Freelancer extends User implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String email;
	private String city;
	private String country;
	private String photo;
	private String cv;
	
	
	@ManyToMany
	private List<Category> categories;
	@OneToMany(mappedBy="freelancer")
	private List<Job> jobsFreelancer;

	@OneToMany(mappedBy="freelancer")
	private List<Sharing> sharings;
	
	@OneToMany(mappedBy="sender")
	private List<AddRequest> addRequestsSender;
	@OneToMany(mappedBy="receiver")
	private List<AddRequest> addRequestsReceiver;
	
	@OneToMany(mappedBy="sender")
	private List<Message> messagesSender;
	@OneToMany(mappedBy="receiver")
	private List<Message> messagesReceiver;
	
	
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getCv() {
		return cv;
	}
	public void setCv(String cv) {
		this.cv = cv;
	}
	public List<Category> getCategories() {
		return categories;
	}
	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
	public List<Job> getJobsFreelancer() {
		return jobsFreelancer;
	}
	public void setJobsFreelancer(List<Job> jobsFreelancer) {
		this.jobsFreelancer = jobsFreelancer;
	}
	public List<Sharing> getSharings() {
		return sharings;
	}
	public void setSharings(List<Sharing> sharings) {
		this.sharings = sharings;
	}
	public List<AddRequest> getAddRequestsSender() {
		return addRequestsSender;
	}
	public void setAddRequestsSender(List<AddRequest> addRequestsSender) {
		this.addRequestsSender = addRequestsSender;
	}
	public List<AddRequest> getAddRequestsReceiver() {
		return addRequestsReceiver;
	}
	public void setAddRequestsReceiver(List<AddRequest> addRequestsReceiver) {
		this.addRequestsReceiver = addRequestsReceiver;
	}
	public List<Message> getMessagesSender() {
		return messagesSender;
	}
	public void setMessagesSender(List<Message> messagesSender) {
		this.messagesSender = messagesSender;
	}
	public List<Message> getMessagesReceiver() {
		return messagesReceiver;
	}
	public void setMessagesReceiver(List<Message> messagesReceiver) {
		this.messagesReceiver = messagesReceiver;
	}
	
	@Override
	public String toString() {
		return getFirstName() + " " + getLastName();
	}

}
