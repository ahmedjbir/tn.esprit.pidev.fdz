package tn.esprit.pidev.fdz.entities;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Job
 *
 */
@Entity

@NamedQueries({
    @NamedQuery(name = "Job.findAll", query = "SELECT j FROM Job j"),
    @NamedQuery(name = "Job.findById", query = "SELECT j FROM Job j WHERE j.id = :id"),
    @NamedQuery(name = "Job.findByTitle", query = "SELECT j FROM Job j WHERE j.title = :title"),
    @NamedQuery(name = "Job.findByDescription", query = "SELECT j FROM Job j WHERE j.description = :description"),
    @NamedQuery(name = "Job.findByRequiredSkills", query = "SELECT j FROM Job j WHERE j.requiredSkills = :requiredSkills"),
    @NamedQuery(name = "Job.findByDuration", query = "SELECT j FROM Job j WHERE j.duration = :duration"),
    @NamedQuery(name = "Job.findByWorkLoad", query = "SELECT j FROM Job j WHERE j.workLoad = :workLoad"),
    @NamedQuery(name = "Job.findByAttachement", query = "SELECT j FROM Job j WHERE j.attachement = :attachement"),
    @NamedQuery(name = "Job.findByRate", query = "SELECT j FROM Job j WHERE j.rate = :rate"),
    @NamedQuery(name = "Job.findByEtat", query = "SELECT j FROM Job j WHERE j.etat = :etat"),
    @NamedQuery(name = "Job.GroupByCategory", query = "SELECT j, count(j.requiredSkills) FROM Job j JOIN j.requiredSkills c Group by(j.requiredSkills)"),
    @NamedQuery(name = "Job.GroupByRate", query = "SELECT j, count(j.rate) FROM Job j Group by(j.rate)")})
public class Job implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String title;
	private String description;
	@ManyToOne
	private Category requiredSkills;
	private int duration;
	private int workLoad;
	private String attachement;
	private double rate;
	private int etat;
	private String isDone = "false";
	@ManyToOne
	private User jobOwner;
	@ManyToOne(optional=true)
	private User freelancer;
	private static final long serialVersionUID = 1L;

	public Job() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}   
	
	public Category getRequiredSkills() {
		return requiredSkills;
	}
	public void setRequiredSkills(Category requiredSkills) {
		this.requiredSkills = requiredSkills;
	}
	public int getDuration() {
		return this.duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}   
	public int getWorkLoad() {
		return this.workLoad;
	}

	public void setWorkLoad(int workLoad) {
		this.workLoad = workLoad;
	}   
	public String getAttachement() {
		return this.attachement;
	}

	public void setAttachement(String attachement) {
		this.attachement = attachement;
	}   
	public double getRate() {
		return this.rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}   
	public int getEtat() {
		return this.etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}
		
	public String isDone() {
		return isDone;
	}
	public void setDone(String isDone) {
		this.isDone = isDone;
	}
	public User getJobOwner() {
		return jobOwner;
	}
	
	public void setJobOwner(User jobOwner) {
		this.jobOwner = jobOwner;
	}
	public User getFreelancer() {
		return freelancer;
	}
	public void setFreelancer(User freelancer) {
		this.freelancer = freelancer;
	}
	
	
   
}
