package tn.esprit.pidev.fdz.entities;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import java.util.Date;

import javax.persistence.*;


/**
 * Entity implementation class for Entity: Sharing
 *
 */
@Entity

@NamedQueries({
    @NamedQuery(name = "Sharing.findAll", query = "SELECT s FROM Sharing s"),
    @NamedQuery(name = "Sharing.findById", query = "SELECT s FROM Sharing s WHERE s.id = :id"),
    @NamedQuery(name = "Sharing.findByContain", query = "SELECT s FROM Sharing s WHERE s.content = :contain"),
    @NamedQuery(name = "Sharing.findByType", query = "SELECT s FROM Sharing s WHERE s.type = :type"),
    @NamedQuery(name = "Sharing.findByDate", query = "SELECT s FROM Sharing s WHERE s.date = :date")})
public class Sharing implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String content;
	private String type;
	private Date date;
	@ManyToOne
    private User freelancer;
	private static final long serialVersionUID = 1L;

	public Sharing() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}   
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}   
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	public User getFreelancer() {
		return freelancer;
	}
	public void setFreelancer(User freelancer) {
		this.freelancer = freelancer;
	}
	
	
   
}
