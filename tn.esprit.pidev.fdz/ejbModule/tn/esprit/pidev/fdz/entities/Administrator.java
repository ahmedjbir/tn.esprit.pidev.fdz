package tn.esprit.pidev.fdz.entities;

import java.io.Serializable;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Entity;

import com.sun.istack.internal.Nullable;
@Entity
@NamedQueries({
	@NamedQuery(name = "Administrator.findByLoginAndPassword", query = "SELECT a FROM Administrator a WHERE a.login = :login and a.password = :password")})

public class Administrator extends User implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Nullable
	private long cin;

	public long getCin() {
		return cin;
	}

	public void setCin(long cin) {
		this.cin = cin;
	}
	
	
	

}
