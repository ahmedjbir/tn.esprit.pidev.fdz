package tn.esprit.pidev.fdz.entities;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;




/**
 * Entity implementation class for Entity: User
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
    @NamedQuery(name = "User.findByLogin", query = "SELECT u FROM User u WHERE u.login = :login")})
public class User implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String login;
	private String password;
	private String firstName;
	private String lastName;
	private String status="locked";
	
	
	@OneToMany(mappedBy="sender")
	private List<Transaction> transactionsSender;
	@OneToMany(mappedBy="receiver")
	private List<Transaction> transactionsReceiver;
	private static final long serialVersionUID = 1L;

	public User() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
		
	
	public void setId(Integer id) {
		this.id = id;
	}
		
			
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<Transaction> getTransactionsSender() {
		return transactionsSender;
	}
	public void setTransactionsSender(List<Transaction> transactionsSender) {
		this.transactionsSender = transactionsSender;
	}
	public List<Transaction> getTransactionsReceiver() {
		return transactionsReceiver;
	}
	public void setTransactionsReceiver(List<Transaction> transactionsReceiver) {
		this.transactionsReceiver = transactionsReceiver;
	}
	
		 

    @Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}
	@Override
    public String toString() {
        return "entities.User[ id=" + id + " ]";
    }
   
}
