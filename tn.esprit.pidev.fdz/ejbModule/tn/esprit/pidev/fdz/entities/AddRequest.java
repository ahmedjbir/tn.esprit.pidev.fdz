package tn.esprit.pidev.fdz.entities;

import java.io.Serializable;
import java.lang.Integer;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: AddRequest
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Addrequest.findAll", query = "SELECT a FROM AddRequest a"),
    @NamedQuery(name = "Addrequest.findById", query = "SELECT a FROM AddRequest a WHERE a.id = :id"),
    @NamedQuery(name = "Addrequest.findByDate", query = "SELECT a FROM AddRequest a WHERE a.date = :date"),
    @NamedQuery(name = "Addrequest.findByEtat", query = "SELECT a FROM AddRequest a WHERE a.etat = :etat")})
public class AddRequest implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private Date date;
	private int etat;
	@ManyToOne
	private User sender;
	@ManyToOne
	private User receiver;
	private static final long serialVersionUID = 1L;

	public AddRequest() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}   
	public int getEtat() {
		return this.etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}
	public User getSender() {
		return sender;
	}
	public void setSender(User sender) {
		this.sender = sender;
	}
	public User getReceiver() {
		return receiver;
	}
	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}
   
}
