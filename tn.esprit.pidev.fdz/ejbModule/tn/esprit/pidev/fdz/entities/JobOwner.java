package tn.esprit.pidev.fdz.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class JobOwner extends User implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String email;
	private String city;
	private String country;
	private String companyName;
	@OneToMany(mappedBy="jobOwner")
	private List<Job> jobsOwner;
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public List<Job> getJobsOwner() {
		return jobsOwner;
	}
	public void setJobsOwner(List<Job> jobsOwner) {
		this.jobsOwner = jobsOwner;
	}
	@Override
	public String toString() {
		return getFirstName() + " " + getLastName();
	}
	
	

}
