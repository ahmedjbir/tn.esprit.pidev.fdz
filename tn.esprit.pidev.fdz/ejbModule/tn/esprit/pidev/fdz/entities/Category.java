package tn.esprit.pidev.fdz.entities;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;



/**
 * Entity implementation class for Entity: Category
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Category.findAll", query = "SELECT c FROM Category c"),
    @NamedQuery(name = "Category.findById", query = "SELECT c FROM Category c WHERE c.id = :id"),
    @NamedQuery(name = "Category.findByName", query = "SELECT c FROM Category c WHERE c.name = :name")})
public class Category implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String name;
	@OneToMany(mappedBy="idCategory")
	private List<Category> subCategories;
	@JoinColumn(name = "idCategory", referencedColumnName = "id")
    @ManyToOne
    private Category idCategory;
	@ManyToMany(mappedBy="categories")
	private List<Freelancer> freelancers;
	@OneToMany(mappedBy="requiredSkills")
	private List<Job> jobs;
	private static final long serialVersionUID = 1L;

	public Category() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public List<Category> getSubCategories() {
		return subCategories;
	}
	public void setSubCategories(List<Category> subCategories) {
		this.subCategories = subCategories;
	}
	public Category getIdCategory() {
		return idCategory;
	}
	public void setIdCategory(Category idCategory) {
		this.idCategory = idCategory;
	}		
   
    public List<Freelancer> getFreelancers() {
		return freelancers ;
	}
	public void setUsers(List<Freelancer> freelancers) {
		this.freelancers = freelancers;
	}
	
	
	public List<Job> getJobs() {
		return jobs;
	}
	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}
	
	
	public void setFreelancers(List<Freelancer> freelancers) {
		this.freelancers = freelancers;
	}
	
	

    @Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
	
    

    @Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}
	@Override
    public String toString() {
        return "entities.Category[ id=" + id + " ]";
    }
}
